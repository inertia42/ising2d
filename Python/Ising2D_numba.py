'''
Author: 刘天成 - 2120200172
Date: 2020.10.17
Version: 1.0

二维Ising模型模拟
无抽象，测试用
'''

import numpy as np
import random
import time
from tqdm import tqdm
import numba as nb


# 初始参数
J = 1.0
T = 0.1 # 初始温度
trys = 1000000
rows = 30
cols = 30

@nb.jit(nopython=True)
def flip(lattice):
    i = random.randrange(rows) #生成随机数
    j = random.randrange(cols)
    r1 = (i-1) % rows #边界条件
    r2 = (i+1) % rows
    c1 = (j-1) % cols
    c2 = (j+1) % cols
    neighbors = np.array([lattice[r1, j], lattice[r2, j], lattice[i, c1], lattice[i, c2]])
    # neighbors = map(int, neighbors)

    nei_sum = 2*(lattice[r1, j] + lattice[r2, j] + 
                lattice[i, c1] + lattice[i, c2])

    # nei_sum = 2*np.sum(neighbors)

    dE = nei_sum if lattice[i, j] == 1 else -nei_sum

    if dE < 0:
        lattice[i, j] = -lattice[i, j]
    else:
        if random.random() < np.exp(-dE/T):
            lattice[i, j] = -lattice[i, j]
    return

if __name__ == "__main__":
    start_time = time.time()
    lattice = 2*np.random.randint(2, size=(rows, cols)) - 1
    print(np.sum(lattice, axis=None)/(rows*cols))
    for i in range(trys):
        flip(lattice)
    print(np.sum(lattice, axis=None)/(rows*cols))
    end_time = time.time()
    print(end_time - start_time, 's\n')
