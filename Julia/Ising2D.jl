#=
Author: inertia
Date: 2020.11.7

测试julia的性能
=#

# 初始参数
J = 1.0
T = 0.1 # 初始温度
trys = 1000000
rows = 30
cols = 30

function flip(lattice)
    i = rand(1:rows) # 生成随机数
    j = rand(1:cols)
    r1 = ((i-2)+30) % rows + 1 # 处理边界条件
    r2 = ((i)+30) % rows + 1
    c1 = ((j-2)+30) % cols + 1
    c2 = ((j)+30) % cols + 1
    neighbors = [lattice[r1, j], lattice[r2, j], lattice[i, c1], lattice[i, c2]]
    
    nei_sum = 2*(lattice[r1, j] + lattice[r2, j] + 
		lattice[i, c1] + lattice[i, c2])
    # nei_sum = sum(lattice)

    if lattice[i, j] == 1 
	dE = nei_sum
    else 
	dE = -nei_sum
    end
    if dE < 0
	lattice[i, j] = -lattice[i, j]
    else
	if rand() < exp(-dE/T)
	    lattice[i, j] = -lattice[i, j]
	end
    end
    return
end

lattice = 2 .* rand(0:1, rows, cols) .- 1 # 生成格点
println(sum(lattice)/(rows*cols)) # 计算平均磁矩

@time for k = 1:trys
    flip(lattice)
end

println(sum(lattice)/(rows*cols))
