'''
Author: 刘天成 - 2120200172
Date: 2020.10.22
Version: 1.0
Requirement: numpy 1.17.0

二维Ising模型模拟
'''

import numpy as np
import random
import time

# 初始参数
J = 1.0
# Temp = 0.1 # 初始温度
trys = 1000000


class IsingBase(object):

    def __init__(self, row: int, col: int, T: float):
        # 初始化网格
        pass
        # self.row = row
        # self.col = col
        # self._lattice = 2*np.random.randint(2, size=(row, col), dtype='int64') - 1
        # self.T = T

    def printer(self):
        # 打印lattice
        for rows in self._lattice:
            for spin in rows:
                if spin == 1:
                    print("+", end=' ')
                else:
                    print('-', end=' ')
            print('\n')
        print('====================以上为一个随机生成的lattice======================\n')
        return

    def get_neighbors(self, row: int, col: int) -> list:
        # 获得近邻
        pass

    @property
    def Partition(self) -> float:
        # 计算配分函数
        partition = 0
        for row in range(self.row):
            for col in range(self.col):
                production = -J*self._lattice[row, col]*sum(self.get_neighbors(row, col))
                partition += np.exp(-(production)/self.T)
        partition = partition/2
        return partition

    def get_summation(self) -> float:
        summation = 0
        for row in range(self.row):
            for col in range(self.col):
                production = -J*self._lattice[row, col]*sum(self.get_neighbors(row, col))
                summation += production * np.exp(-(production)/self.T)
        summation = summation/2
        return summation

    def get_summation2(self) -> float:
        summation2 = 0
        for row in range(self.row):
            for col in range(self.col):
                production = -J*self._lattice[row, col]*sum(self.get_neighbors(row, col))
                summation2 += (production**2) * np.exp(-(production)/self.T)
        summation2 = summation2/2
        return summation2

    def flip(self):
        # 进行翻转操作
        i = random.randrange(self.row)
        j = random.randrange(self.col)

        nei_sum = 2*sum(self.get_neighbors(i, j))
        dE = nei_sum if self._lattice[i, j] == 1 else -nei_sum

        if dE < 0:
            self._lattice[i, j] = -self._lattice[i, j]
        else:
            if random.random() < np.exp(-dE/self.T):
                self._lattice[i, j] = -self._lattice[i, j]
        return

    @property
    def Mag(self) -> float:
        # 计算平均磁矩
        return np.sum(self._lattice, axis=None)/(self.row*self.col)

    def show(self):
        # 打印最终结果
        start_time = time.time()
        print('==============温度为' + str(self.T) + '的情况==============')
        print('初始平均磁矩为：' + str(self.Mag))
        print('初始格点的配分函数为：' + str(self.Partition))
        for i in range(trys):
            self.flip()
        print('翻转后平均磁矩为：' + str(Ising2D.Mag))
        end_time = time.time()
        print(end_time - start_time, 's\n')
        return


class Ising(IsingBase):

    def __init__(self, row: int, col: int, T: float):
        # 初始化网格
        self.row = row
        self.col = col
        self._lattice = 2*np.random.randint(2, size=(row, col), dtype='int64') - 1
        self.T = T

    def get_neighbors(self, row: int, col: int) -> list:
        # 获取最近邻
        r1 = (row-1) % self.row
        r2 = (row+1) % self.row
        c1 = (col-1) % self.col
        c2 = (col+1) % self.col
        return [self._lattice[r1, col], self._lattice[r2, col],
                self._lattice[row, c1], self._lattice[row, c2]]


class IsingMoreN(IsingBase):

    def __init__(self, row: int, col: int, T: float, J1: float):
        # 初始化网格
        self.row = row
        self.col = col
        self._lattice = 2*np.random.randint(2, size=(row, col), dtype='int64') - 1
        self.T = T
        self.J = J1

    def get_neighbors(self, row: int, col: int) -> list:
        # 获取最近邻和次近邻
        r1 = (row-1) % self.row
        r2 = (row+1) % self.row
        c1 = (col-1) % self.col
        c2 = (col+1) % self.col
        J2 = self.J/J
        return [self._lattice[r1, col], self._lattice[r2, col],
                self._lattice[row, c1], self._lattice[row, c2],
                J2*self._lattice[r1, c1], J2*self._lattice[r1, c2],
                J2*self._lattice[r2, c1], J2*self._lattice[r2, c2]]

    def show(self):
        start_time = time.time()
        print("==========参数J'为" + str(self.J) + "  温度为" + str(self.T) + "的情况==============")
        print('初始平均磁矩为：' + str(self.Mag))
        print('初始格点的配分函数为：' + str(self.Partition))
        for i in range(trys):
            self.flip()
        print('翻转后平均磁矩为：' + str(Ising2D.Mag))
        end_time = time.time()
        print(end_time - start_time, 's\n')
        return


if __name__ == "__main__":
    start_time = time.time()
    Ising2D = Ising(30, 30, 0.5)
    Ising2D.printer()
    Ising2D.show()
    Ising2D = Ising(30, 30, 1)
    Ising2D.show()
    Ising2D = Ising(30, 30, 2)
    Ising2D.show()
    Ising2D = Ising(30, 30, 3)
    Ising2D.show()
    Ising2D = Ising(30, 30, 5)
    Ising2D.show()
    Ising2D = IsingMoreN(30, 30, 1, -0.7)
    Ising2D.show()
    Ising2D = IsingMoreN(30, 30, 1, -0.2)
    Ising2D.show()
    Ising2D = IsingMoreN(30, 30, 1, 0.2)
    Ising2D.show()
    Ising2D = IsingMoreN(30, 30, 1, 0.7)
    Ising2D.show()






